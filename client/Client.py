#!/usr/bin/env python

import sys
sys.path.insert(0, '../protocol/')
sys.path.insert(0, '../')

from Message import Message
from Protocol import Protocol
from Config import Config

from getpass import getpass
import socket
import ssl
import hashlib

class Client(object):
    def __init__(self):
        # create TCP socket with ssl using trusted certificate
        # self.__sock_nas = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM), ca_certs=certfile, cert_reqs=ssl.CERT_REQUIRED, ssl_version=ssl.PROTOCOL_TLSv1)
        self.__sock_server = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM),
                                                    ca_certs=Config.ssl_cert,
                                                    cert_reqs=ssl.CERT_REQUIRED,
                                                    ssl_version=ssl.PROTOCOL_TLSv1)
        self.__session_id = None

    def disconnect(self):
        self.__sock_server.close()

    def login(self, username, password, ip, port):
        """
        Sends login request to server NAS and checks the response

        @param username: string with username that will be validated on server
        @param password: string with password that will be validated on server
        @param ip: string with ip server which the client wants to connect
        @param port: number with port server which the clients wants to connect
        @return: True if authentication was successful else False
        """
        nas_socket = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM),
                                                    ca_certs=Config.ssl_cert,
                                                    cert_reqs=ssl.CERT_REQUIRED,
                                                    ssl_version=ssl.PROTOCOL_TLSv1)
        
        nas_socket.connect((Config.NAS_IP, Config.NAS_PORT))
        m = Message(Protocol.LOGIN)
        m.pack(username)
        m.pack(hashlib.sha256(password).hexdigest())
        m.pack(ip)
        m.pack(port)
        Protocol.send_message(nas_socket, m)
        response = Protocol.read_message(nas_socket).unpack()
        nas_socket.close()
        if response[0] == Protocol.SUCCESS:
            self.__session_id = response[1]
            return True
        elif response[0] == Protocol.DENIED:
            print 'Access denied !'
            return False
        elif response[0] == Protocol.ERROR:
            print ''.join(response[1:])
            return False


    def auth(self, ip, port):
        """
        Sends authentication packet to server (Bob/Cecylia) and checks the response

        @param ip: string with ip server which the client wants to connect
        @param port: number with port server which the clients wants to connect
        @return: True if authentication was successful else False
        """
        if self.__session_id != None:
            self.__sock_server.connect((ip, port))
            m = Message(Protocol.AUTH)
            m.pack(self.__session_id)
            m.pack(ip)
            Protocol.send_message(self.__sock_server, m)
            response = Protocol.read_message(self.__sock_server).unpack()
            if response[0] == Protocol.SUCCESS:
            	return True            
            elif response[0] == Protocol.DENIED:
                print response[1]
                self.__session_id = None
                return False
        else:
            print 'You don\'t have session ID !'
            return False
    
    def show_files(self):
        """
        Method which allow user to see avaiable files on the server
        """
        m = Message(Protocol.FILE_SHOW)
        Protocol.send_message(self.__sock_server, m)
        response = Protocol.read_message(self.__sock_server).unpack()
        
        if response[0] == Protocol.SUCCESS:
            notes = response[1:]
            for note in notes:
                print note + ' '
            return True
        elif response[0] == Protocol.ERROR:
            print ''.join(response[1:])
            return False
        
    def read_file(self, filename):
        """
        Method which allow user to read file from server
        @param filename: string with name of desired file
        """
        m = Message(Protocol.FILE_READ)
        m.pack(filename)
        Protocol.send_message(self.__sock_server, m)
        response = Protocol.read_message(self.__sock_server).unpack()
        
        if response[0] == Protocol.SUCCESS:
            notes = response[1:]
            for note in notes:
                print note
            return True
        elif response[0] == Protocol.DENIED:
            print response[1]
            return false
        elif response[0] == Protocol.ERROR:
            print ''.join(response[1:])
            return False

    def edit_file(self, filename):
        """
        Method which allow user to add text to file from server
        @param filename: string with name of desired file
        """
        new_content = raw_input("Write content to add it to file: ")
        m = Message(Protocol.FILE_EDIT)
        m.pack(filename)
        m.pack(new_content)
        Protocol.send_message(self.__sock_server, m)
        response_edit = Protocol.read_message(self.__sock_server).unpack()

        if response_edit[0] == Protocol.SUCCESS:
            return True
        elif response_edit[0] == Protocol.ERROR:
            print ''.join(response_edit[1:])
            return False
        elif response_edit[0] == Protocol.DENIED:
            print 'Acces denied'
            return False

def print_usage():
    print '@arg1: Server ip address\n' \
          '@arg2: Server port number'

def print_help_nas():
    print 'commands:\n' \
          'help: prints this message\n' \
          'log in: for authentication and starting session\n' \
          'exit: to exit'
          
def print_help_server():
    print 'commands:\n' \
          'help: prints this message\n' \
          'show: for see avaiable files on the server\n' \
          'read: for read file\n' \
          'edit: for add some text to file\n' \
          'exit: to logout and exit'

def logged_in_loop(c, ip, port): 
    """
    Method which is used when user is connected with server (Bob/Cecylia); First step is authentication with it
    @param ip: string with ip server which the client wants to connect
    @param port: number with port server which the clients wants to connect
    """   
    if c.auth(ip, port) == True:
        print_help_server()
        
        while True:
            command = raw_input("Your command: ")
            
            if command == 'help':
                 print_help_server()
            elif command == 'show':
                c.show_files();
            elif command == 'read':
                file = raw_input("filename: ")
                c.read_file(file);
            elif command == 'edit':
                file = raw_input("filename: ")
                c.edit_file(file);
            elif command == 'exit':
                return 'exit'
            else:
                print 'Wrong command'
    else:
        return False

def main():
    #if len(sys.argv) != 3:
     #   print_usage()
      #  exit()

    c = Client()
    try:
        print_help_nas()
        # logged_in_loop(c, 1,1)
        while True:
            command = raw_input("Your command: ")

            if command == 'help':
                print_help_nas()
            elif command == 'log in':
                username = raw_input("username: ")
                password = getpass("password: ")
                serv = raw_input("which server? (c -> cecylia, b -> bob): ")

                if serv == 'c':
                    ip = Config.CECYLIA_IP
                    port = Config.CECYLIA_PORT
                elif serv == 'b':
                    ip = Config.BOB_IP
                    port = Config.BOB_PORT
                else:
                    print 'wrong server'
                    break
                
                if c.login(username, password, ip, port):
                    print 'logged in as ', username
                    if logged_in_loop(c, ip, port) == 'exit':
                        break
            elif command == 'exit':
                break
            else:
                print 'Wrong command'

    except Exception as e:
        print e.message
    except ValueError:
        print 'Wrong port number'
    finally:
        c.disconnect()

if __name__ == '__main__':
    main()
