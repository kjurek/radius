class Config(object):
	RADIUS_IP = 'kjurek.no-ip.org'
	RADIUS_PORT = 1812
	RADIUS_SECRET = 'testing123'
	NAS_IP = '127.0.0.1'
	NAS_PORT = 10000
	NAS_IDENTIFIER = 'localhost'
	BOB_IP = '127.0.0.1'
	BOB_PORT = 20000
	CECYLIA_IP = '127.0.0.1'
	CECYLIA_PORT = 30000

	ssl_cert = '../certs/ssl.crt'
	ssl_key = '../certs/ssl.key'