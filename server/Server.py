#!/usr/bin/env python

import sys
sys.path.insert(0, '../protocol/')
sys.path.insert(0, '../')

import socket
import ssl
import threading

from ClientHandler import ClientHandler
from NasHandler import NasHandler
from Database import Database
from Config import Config

class ClientThread(threading.Thread):
    def __init__(self, ip, port, database):
        threading.Thread.__init__(self)
        self.database = database
        self.sock_client = socket.socket()
        self.sock_client.bind((ip, port))
        self.sock_client.listen(5)

    def run(self):
        try:
            while True:
                client_socket, client_addr = self.sock_client.accept()
                ssl_client_socket = ssl.wrap_socket(client_socket,
                                            server_side=True,
                                            certfile=Config.ssl_cert,
                                            keyfile=Config.ssl_key,
                                            ssl_version=ssl.PROTOCOL_TLSv1)
                # handle connection from Client
                ClientHandler(ssl_client_socket, client_addr, self.database).start()
        finally:
            self.sock_client.close()

class NasThread(threading.Thread):
    def __init__(self, ip, port, database):
        threading.Thread.__init__(self)
        self.database = database
        self.sock_client = socket.socket()
        self.sock_client.bind((ip, port))
        self.sock_client.listen(5)
        
    def run(self):
        try:
            while True:
                client_socket, client_addr = self.sock_client.accept()
                ssl_client_socket = ssl.wrap_socket(client_socket,
                                            server_side=True,
                                            certfile=Config.ssl_cert,
                                            keyfile=Config.ssl_key,
                                            ssl_version=ssl.PROTOCOL_TLSv1)
                # handle connection from NAS
                NasHandler(ssl_client_socket, self.database).start()
        finally:
            self.sock_client.close()

class Server(object):
    def __init__(self, ip, port, db_file_path='SecureServer.db'):
        self.__port = port
        self.__ip = ip
        self.__database = Database(db_file_path)

    def run(self):
        print 'starting server'
        ct = ClientThread(self.__ip, self.__port, self.__database)
        nt = NasThread(self.__ip, self.__port + 1, self.__database)
        ct.start()
        nt.start()
        ct.join()
        nt.join()
        
def print_usage():
    print '@arg1: bob/cecylia'

def main():
    if len(sys.argv) != 2:
        print_usage()
        exit()

    try:
        if sys.argv[1] == 'bob':
            ip, port = (Config.BOB_IP, Config.BOB_PORT)
        elif sys.argv[1] == 'cecylia':
            ip,port = (Config.CECYLIA_IP, Config.CECYLIA_PORT)
        else:
            print_usage()
            exit()

        s = Server(ip, port)
        s.run()
    except ValueError:
        print 'Invalid port number'
    

if __name__ == '__main__':
    main()
