from Message import Message

class Protocol(object):
    ERROR = '0'
    SUCCESS = '1'
    DENIED = '2'
    LOGIN = '3'
    SESSION_REQUEST = '4'
    SESSION_RESPONSE = '5'
    FILE_SHOW = '6'
    FILE_READ = '7'
    FILE_EDIT = '8'
    LOGOUT = '9'
    AUTH = '10'

    @staticmethod
    def send_message(sock, message):
        """
        Sends message using this Protocol

        @param sock: socket for communication
        @param message: Message object that contains packed data to send with sock
        """
        # get packed message as string and calculate it's length
        sock.send(message.get())

    @staticmethod
    def read_message(sock):
        """
        Reads message using this protocol

        @param sock: socket for communication
        @return: returns Message object that contains packed data read from sock
        @raise Exception: it occurs when connection is broken
        """
        data = sock.recv(1024)
        if len(data) == 0:
            raise Exception('wrong message or user disconnected')
        m = Message()
        m.set(data)
        return m
    
