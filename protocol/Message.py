class Message(object):
    def __init__(self, message_type=None):
        if message_type is not None:
            self.__message = '{}'.format(message_type)
        else:
            self.__message = ''

    def pack(self, buff):
        """
        Packs piece of data into message separating it with ^ character

        @param buff: data to pack
        """
        self.__message = '{}^{}'.format(self.__message, buff)

    def unpack(self):
        """
        @return: Returns unpacked message as list
        """
        return self.__message.split('^')

    def get(self):
        """
        @return: Returns packed data
        """
        return self.__message

    def set(self, new_message_str):
        """
        Sets new data

        @param new_message_str: new packed message
        """
        self.__message = new_message_str