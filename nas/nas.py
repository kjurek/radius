#!/usr/bin/env python

import sys
sys.path.insert(0, '../protocol/')
sys.path.insert(0, '../')

import pyrad.packet
import threading
import socket
import ssl

from Protocol import Protocol
from Message import Message
from pyrad.client import Client
from pyrad.dictionary import Dictionary
from Config import Config

class NasClientHandler(threading.Thread):
    def __init__(self, sock, addr, radius_ip, radius_secret, nas_identifier, bob_ip, bob_port, cecylia_ip, cecylia_port):
        threading.Thread.__init__(self)
        self.sock = sock
        self.radius_ip = radius_ip
        self.radius_secret = radius_secret
        self.nas_identifier = nas_identifier
        self.bob_ip = bob_ip
        self.bob_port = bob_port
        self.cecylia_ip = cecylia_ip
        self.cecylia_port = cecylia_port
        self.addr = addr

    def run(self):
        m = Protocol.read_message(self.sock).unpack()
        # handle login message
        if m[0] == Protocol.LOGIN:
            self.handle_login(m)
        self.sock.close()
            
    def handle_login(self, unpacked_message):
        username = unpacked_message[1]
        password = unpacked_message[2]
        
        # ip and port of bob/cecylia
        server_ip = unpacked_message[3]
        server_port = int(unpacked_message[4])
        requested_server = ''
        
        if (server_ip, server_port) == (self.bob_ip, self.bob_port):
            requested_server = 'Bob'
        elif (server_ip, server_port) == (self.cecylia_ip, self.cecylia_port):
            requested_server = 'Cecylia'

        print 'Requested server: ', server_ip, server_port
        # authenticate and authorize with radius
        r = self.authorize_with_radius(username, password)
        # r contains list of servers that user can connect to
        print 'got reply from radius: ', r
        if r:
            if requested_server in r:
                print 'Sending session request to ', requested_server
                # create session for user on bob/cecylia
                self.send_session_request(server_ip, server_port, username)
            else:
                m = Message(Protocol.DENIED)
                Protocol.send_message(self.sock, m)
        else:
            m = Message(Protocol.DENIED)
            Protocol.send_message(self.sock, m)

    def send_session_request(self, ip, port, username):
        s = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM),
                                                    ca_certs=Config.ssl_cert,
                                                    cert_reqs=ssl.CERT_REQUIRED,
                                                    ssl_version=ssl.PROTOCOL_TLSv1)
        s.connect((ip,port+1))
        m = Message(Protocol.SESSION_REQUEST)
        m.pack(username)
        m.pack(self.addr[0])
        Protocol.send_message(s, m)
        response = Protocol.read_message(s).unpack()
        print response
        s.close()
        print 'got response from requested server'
        if response[0] == Protocol.SESSION_RESPONSE:
            print 'session created! sending message to client'
            m = Message(Protocol.SUCCESS)
            m.pack(response[1])
            Protocol.send_message(self.sock, m)
        elif response[0] == Protocol.ERROR:
            print 'Error!'
            m = Message(Protocol.ERROR)
            m.pack(response[1])
            Protocol.send_message(self.sock, m)

    def authorize_with_radius(self, username, password):
        srv=Client(server=self.radius_ip, secret=self.radius_secret,
              dict=Dictionary("dictionary"))

        req=srv.CreateAuthPacket(code=pyrad.packet.AccessRequest,
                      User_Name=username, NAS_Identifier=self.nas_identifier)
        # password is hashed earlier in client with sha256, and now it's encrypted with secret key
        req["User-Password"]=req.PwCrypt(password)

        reply=srv.SendPacket(req)
        if reply.code==pyrad.packet.AccessAccept:
            print 'radius sent accept!'
            # radius replies with list of servers that user is allowed to connect
            return reply.values()[0][0].split(',')
        else:
            print 'radius sent reject!'
            return None

class Nas(object):
    def __init__(self, nas_ip = Config.NAS_IP, nas_port=Config.NAS_PORT, radius_ip=Config.RADIUS_IP, radius_secret=Config.RADIUS_SECRET, nas_identifier = Config.NAS_IDENTIFIER, bob_ip = Config.BOB_IP, bob_port = Config.BOB_PORT, cecylia_ip= Config.CECYLIA_IP, cecylia_port=Config.CECYLIA_PORT):
        self.nas_ip = nas_ip
        self.nas_port = nas_port
        self.radius_ip = radius_ip
        self.radius_secret = radius_secret
        self.nas_identifier = nas_identifier
        self.bob_ip = bob_ip
        self.bob_port = bob_port
        self.cecylia_ip = cecylia_ip
        self.cecylia_port = cecylia_port
        
    def run(self):
        print 'starting nas'
        try:
            # create socket and bind it to given ip and port
            sock_client = socket.socket()
            sock_client.bind((self.nas_ip, self.nas_port))
            sock_client.listen(5)

            while True:
                # get incoming connection socket
                client_socket, client_addr = sock_client.accept()
                ssl_client_socket = ssl.wrap_socket(client_socket,
                                            server_side=True,
                                            certfile=Config.ssl_cert,
                                            keyfile=Config.ssl_key,
                                            ssl_version=ssl.PROTOCOL_TLSv1)
                NasClientHandler(ssl_client_socket, client_addr, self.radius_ip, self.radius_secret, self.nas_identifier, self.bob_ip, self.bob_port, self.cecylia_ip, self.cecylia_port).start()
        finally:
            sock_client.close()

def main():
    n = Nas()
    n.run()

if __name__ == '__main__':
    main()