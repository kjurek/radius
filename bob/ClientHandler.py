import socket
import re
import threading

from Exceptions import DatabaseException
from Message import Message
from Exceptions import MessageException
from Protocol import Protocol
from Exceptions import SessionException

class ClientHandler(threading.Thread):
    def __init__(self, sock, addr, db):
        """

        @param sock: SSL wrapped socket for communication with connected client
        @param db: sqlite3 database cursor created by Server
        """
        threading.Thread.__init__(self)
        self.__sock = sock
        self.__addr = addr
        self.__database = db

        # id of current user session (initial value is none, it will be assigned when client will successfuly log in)
        self.__session_id = None

    def run(self):
        try:
            msg = Protocol.read_message(self.__sock)
            unpacked_msg = msg.unpack()
            if unpacked_msg[0] == Protocol.AUTH:
                session_id = unpacked_msg[1]
                result_auth = self.__auth(session_id, self.__addr[0])
                if result_auth == True:
                    while True:
                        # wait for message from client and save it's as Message object
                        msg = Protocol.read_message(self.__sock)
                        unpacked_msg = msg.unpack()
                        # depending on type of received message do proper actions
                        if unpacked_msg[0] == Protocol.FILE_SHOW:
                            self.__handle_file_show(unpacked_msg)
                        elif unpacked_msg[0] == Protocol.FILE_READ:
                            filename = unpacked_msg[1]
                            self.__handle_file_read(unpacked_msg)
                        elif unpacked_msg[0] == Protocol.FILE_EDIT:
                            filename = unpacked_msg[1]
                            print 'server: ', filename
                            self.__handle_file_edit(unpacked_msg)
                        elif unpacked_msg[0] == Protocol.LOGOUT:
                            self.__handle_logout(unpacked_msg)
                        else:
                            raise MessageException('Unknown message')

        # these are critical exceptions and when they occur, there is need to close connection with client
        except DatabaseException as e:
            print e.msg
        except SessionException as e:
            print e.msg
        except MessageException as e:
            print e.msg
        except socket.error as e:
            print e.strerror
        except Exception as e:
            print e.message
        finally:
            # if connection was broken by client or another crtitical exception occured, session must be deleted from database
            if self.__session_id is not None:
                self.__database.delete_session(self.__session_id)
                self.__session_id = None
            self.__sock.shutdown(socket.SHUT_RDWR)
            self.__sock.close()

    def __auth(self, session_id, ip):
        print 'checking session id and ip for ', session_id, ' ', ip
        result_auth = self.__database.check_session_id_and_ip(session_id, ip)
        if result_auth == True:
            m = Message(Protocol.SUCCESS)
            self.__session_id = session_id
            Protocol.send_message(self.__sock, m)
            return True
        elif result_auth == False:
            m = Message(Protocol.DENIED)
            m.pack('Your session ID or IP is incorrect !')
            Protocol.send_message(self.__sock, m)
            return False
        elif result_auth == None:
            m = Message(Protocol.DENIED)
            m.pack('This IP is not exists in database !')
            Protocol.send_message(self.__sock, m)
            return False

    def __handle_file_show(self, unpacked_msg):
        if len(unpacked_msg) != 1:
            raise MessageException('Recived invalid message, disconnecting user')
        
        files = self.__database.get_files_list(self.__session_id)
        if files is not None:
            m = Message(Protocol.SUCCESS)
            for file in files:
                if file[1] == 1:
                    m.pack('{} r'.format(file[0]))
                elif file[1] == 2:
                    m.pack('{} rw'.format(file[0]))
            Protocol.send_message(self.__sock, m)
        else:
            m = Message(Protocol.ERROR)
            m.pack('You have no access for files')
            Protocol.send_message(self.__sock, m)
            
    def __handle_file_read(self, unpacked_msg):
        if len(unpacked_msg) != 2:
            raise MessageException('Recived invalid message, disconnecting user')
        
        filename = unpacked_msg[1]
        rights = self.__database.check_user_rights(self.__session_id, filename);
        
        if rights is not None:
            if rights != 0:
                m = Message(Protocol.SUCCESS)
                content = self.__get_content_from_file(filename)
                m.pack(content)
                Protocol.send_message(self.__sock, m)
            else:
                m = Message(Protocol.DENIED)
                m.pack('You have no access to read this file !')
                Protocol.send_message(self.__sock, m)
        else:
            m = Message(Protocol.ERROR)
            m.pack('Filename is incorreact')
            Protocol.send_message(self.__sock, m)
        
    def __handle_file_edit(self, unpacked_msg):
        if len(unpacked_msg) != 3:
            raise MessageException('Recived invalid message, disconnecting user')

        filename = unpacked_msg[1]
        new_content = unpacked_msg[2]
        rights = self.__database.check_user_rights(self.__session_id, filename);
        if rights is not None:
            if rights == 2:
                if self.__add_content_from_file(filename, new_content) == True:
                     m = Message(Protocol.SUCCESS)
                     m.pack('Content successfully added to file !')
                     Protocol.send_message(self.__sock, m)
                else:
                    m = Message(Protocol.ERROR)
                    m.pack('Error during add content !')
                    Protocol.send_message(self.__sock, m)
            else:
                m = Message(Protocol.DENIED)
                m.pack('You have no access to edit this file !')
                Protocol.send_message(self.__sock, m)
        else:
            m = Message(Protocol.ERROR)
            m.pack('Filename is incorrect')
            Protocol.send_message(self.__sock, m)

    def __handle_logout(self, unpacked_msg):
        """

        @param unpacked_msg: list of arguments sent by client, in this case (Protocol.LOGOUT)
        @raise MessageException: if message is invalid (for example someone wrote his own client program and tries to send wrong packets)
                                 the connections is closed
        """

        # number of arguments in unpacked message must be 1 (Protocol.LOGOUT)
        if len(unpacked_msg) != 1:
                raise MessageException('Recived invalid message, disconnecting user')

        # checks if user is logged in, if yes then it's deleting current session from database
        if self.__session_id is not None:
            self.__database.delete_session(self.__session_id)
            self.__session_id = None
            Protocol.send_message(self.__sock, Message(Protocol.SUCCESS))
        else:
            m = Message(Protocol.ERROR)
            m.pack('You are not logged in')
            Protocol.send_message(self.__sock, m)
            
    def __get_content_from_file(self, filename):
        file = open('files/' + filename + '.txt')
        content = ''.join(file.readlines())
        file.close()
        return content
    
    def __add_content_from_file(self, filename, new_content):
        file = open('files/' + filename + '.txt', 'a')
        file.write(new_content)
        file.close();
        return True
