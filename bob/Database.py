from Exceptions import DatabaseException

import binascii
import datetime
import hashlib
import os
import sqlite3
import random
from threading import Lock

class Database(object):
    def __init__(self, db_filename):
        """

        @param db_filename: Path or name to database file
        """
        self.__db_filename = db_filename

        # check if database was created earlier
        if not os.path.isfile(db_filename):
            # if not then create all tables
            self.__create_db(self.__db_filename)
        else:
            # if it exists then connect to it and clear session table
            self.__database = sqlite3.connect(self.__db_filename, check_same_thread=False)
            self.__drop_session_table()
            self.__create_session_table()
        # lock is needed because sqlite3 does not support multithreading (it's not safe)
        self.__mutex = Lock()

    def register_session(self, username, ip):
        """
        Registers new session

        @param username: string with valid username
        @return: generated session_id or None if username is assigned to another session
        @raise DatabaseException: it occurs when database is designed wrong or there is no connection with database
        """
        try:
            with self.__database as db:
                self.__mutex.acquire()
                print 'starting register session in db'
                # get user id by username
                users = db.execute("select * from User").fetchall()
                for user in users:
                    print user
                user_id = db.execute("SELECT ID FROM User WHERE Username LIKE ?", (username, )).fetchone()[0]
                print 'user id: ', user_id
                # check if username is not used by another session
                rows = db.execute("SELECT Session_ID FROM Session WHERE User_ID=?", (user_id, )).fetchall()
                print 'sessions ', rows
                if len(rows) > 0:
                    return None

                while True:
                    # generate new session id random string
                    session_id = binascii.hexlify(os.urandom(32))

                    # check if there is another session with this generated id (very low possibility)
                    rows = db.execute("SELECT Session_ID FROM Session WHERE Session_ID LIKE ?", (session_id, )).fetchall()
                    if len(rows) == 0:
                        break

                # save new session id and assign user to it
                db.execute("INSERT INTO Session(Session_ID, User_ID, IP) VALUES(?,?,?)", (session_id, user_id, ip))
                return session_id
        except sqlite3.Error as e:
            raise DatabaseException('Could not save session for user {} ({})'.format(username, e.message))
        finally:
            self.__mutex.release()

    def delete_session(self, session_id):
        """
        Deletes session

        @param session_id: string with earlier generated session id
        @raise DatabaseException: it occurs when database is designed wrong or there is no connection with database
        """
        try:
            with self.__database as db:
                self.__mutex.acquire()

                # remove from database this session
                db.execute("DELETE FROM Session WHERE Session_ID LIKE ?", (session_id, ))
        except sqlite3.Error as e:
            raise DatabaseException('Could not delete session {} ({})'.format(session_id, e.message))
        finally:
            self.__mutex.release()
            
    def check_session_id_and_ip(self, session_id, ip):
        try:
            with self.__database as db:
                self.__mutex.acquire()

                # get ip from session with given session id
                ip_from_db = db.execute("SELECT IP FROM Session WHERE Session_ID LIKE ?", (session_id, )).fetchone()
                
                if ip_from_db is not None:
                    if ip_from_db[0] == ip:
                        return True
                    else:
                        return False
                else:
                    return None

        except sqlite3.Error as e:
            raise DatabaseException(e.message)
        finally:
            self.__mutex.release()
    
    def get_files_list(self, session_id):
        try:
            with self.__database as db:
                self.__mutex.acquire()

                # get user id from session with given session id
                user_id = db.execute("SELECT User_id FROM Session WHERE Session_ID LIKE ?", (session_id, )).fetchone()[0]
                
                # save note and date
                rows = db.execute("SELECT File, Rights FROM Privilages WHERE User_ID=?", (user_id, )).fetchall()

                if len(rows) > 0:
                    return rows
                else:
                    return None

        except sqlite3.Error as e:
            raise DatabaseException('Could not get files list for user with id {} from session {} ({})'.format(user_id, session_id, e.message))
        finally:
            self.__mutex.release()
            
    def check_user_rights(self, session_id, filename):
        try:
            with self.__database as db:
                self.__mutex.acquire()

                # get user id from session with given session id
                user_id = db.execute("SELECT User_id FROM Session WHERE Session_ID LIKE ?", (session_id, )).fetchone()[0]
                # save note and date
                rights = db.execute("SELECT Rights FROM Privilages WHERE User_ID=? AND File = ?", (user_id, filename )).fetchone()
                
                if rights is not None:
                    return rights[0]
                else:
                    return None

        except sqlite3.Error as e:
            raise DatabaseException('Filename is incorrect')
        finally:
            self.__mutex.release()

    def __create_db(self, db_filename):
        """
        Creates new database, with user and session tables

        @param db_filename: path where new database will be created
        """
        self.__database = sqlite3.connect(db_filename, check_same_thread=False)
        self.__create_user_table()
        self.__create_user_privilages_table()
        self.__create_session_table()

    def __create_user_table(self):
        """
        Creates user table that consists of (user id, username)

        @raise DatabaseException: occurs when connection with database is not available
        """
        try:
            with self.__database as db:
                db.execute("CREATE TABLE User(ID INTEGER PRIMARY KEY, Username VARCHAR UNIQUE)")
                
                username_list = ['admin', 'olek', 'kjurek', 'tfilip', 'pyt', 'abc', 'bat3']
                for username in username_list:
                    db.execute("INSERT INTO User(Username) VALUES(?)", (username,))
        except sqlite3.Error as e:
            raise DatabaseException("Could not create table user ({})".format(e.message))

    def __create_user_privilages_table(self):
        """
        Creates user table that consists of (user id, file, privilages)

        @raise DatabaseException: occurs when connection with database is not available
        """
        try:
            with self.__database as db:
                db.execute("CREATE TABLE Privilages(User_ID INTEGER, File VARCHAR, Rights INTEGER, FOREIGN KEY(User_ID) REFERENCES User(ID))")
                username_list = ['admin', 'olek', 'kjurek', 'tfilip', 'pyt', 'abc', 'bat3']
                files_list = ['file1', 'file2', 'file3', 'file4', 'file5', 'file6', 'file7', 'file8']
                for username in username_list:
                    user_id = db.execute("SELECT ID FROM User WHERE Username LIKE ?", (username,)).fetchone()[0]
                    for file in files_list:
                        rights = random.sample(range(0,3), 1)[0]
                        db.execute("INSERT INTO Privilages(User_ID, File, Rights) VALUES(?,?,?)", (user_id, file, rights))
        except sqlite3.Error as e:
            raise DatabaseException("Could not create table privilages ({})".format(e.message))

    def __create_session_table(self):
        """
        Creates session table that consists of (sessiond id, user id)

        @raise DatabaseException: occurs when connection with database is not available
        """
        try:
            with self.__database as db:
                db.execute("CREATE TABLE Session(Session_ID VARCHAR UNIQUE, IP VARCHAR, User_ID INTEGER UNIQUE, FOREIGN KEY(User_ID) REFERENCES User(ID))")
        except sqlite3.Error as e:
            raise DatabaseException("Could not create table session ({})".format(e.message))

    def __drop_session_table(self):
        """
        Deletes session table

        @raise DatabaseException: occurs when connection with database is not available
        """
        try:
            with self.__database as db:
                db.execute("DROP TABLE IF EXISTS Session")
        except sqlite3.Error as e:
            raise DatabaseException("Could not drop table session ({})".format(e.message))

