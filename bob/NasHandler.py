import socket
import re
import threading

from Exceptions import DatabaseException
from Message import Message
from Exceptions import MessageException
from Protocol import Protocol
from Exceptions import SessionException

class NasHandler(threading.Thread):
    def __init__(self, sock, db):
        """

        @param sock: SSL wrapped socket for communication with connected client
        @param db: sqlite3 database cursor created by Server
        """
        threading.Thread.__init__(self)
        self.__sock = sock
        self.__database = db

    def run(self):
        try:
            # while True:
            # wait for message from client and save it's as Message object
            print 'waiting for message'
            
            msg = Protocol.read_message(self.__sock)
            unpacked_msg = msg.unpack()

            # depending on type of received message do proper actions
            if unpacked_msg[0] == Protocol.SESSION_REQUEST:
                print 'got session request'
                self.__handle_session_request(unpacked_msg)
            else:
                raise MessageException('Unknown message')

        # these are critical exceptions and when they occur, there is need to close connection with client
        except DatabaseException as e:
            print e.msg
        except SessionException as e:
            print e.msg
        except MessageException as e:
            print e.msg
        except socket.error as e:
            print e.strerror
        except Exception as e:
            print e.message
        finally:
            self.__sock.shutdown(socket.SHUT_RDWR)
            self.__sock.close()


    def __handle_session_request(self, unpacked_msg):
        """

        @param unpacked_msg: list of arguments sent by client, in this case (Protocol.LOGIN, username, password)
        @return: @raise MessageException: if message is invalid (for example someone wrote his own client program and tries to send wrong packets)
                                          the connections is closed
        """

        # number of arguments in unpacked message must be 3 (Protocol.LOGIN, username, password)
        if len(unpacked_msg) != 3:
            raise MessageException('Received invalid message, disconnecting user')

        username = unpacked_msg[1]
        ip = unpacked_msg[2]
        print 'handling session request for ', username, ' ', ip

         # assign session id to this connection and save it in database
        session_id = self.__database.register_session(username, ip)
        print 'registering new session'
         # if there is some one else logged on given user, respond with error message
        if session_id is None:
            m = Message(Protocol.ERROR)
            m.pack("Account already in use")
            Protocol.send_message(self.__sock, m)
            return
        else:
             m = Message(Protocol.SESSION_RESPONSE)
             m.pack(session_id)
             Protocol.send_message(self.__sock, m)
